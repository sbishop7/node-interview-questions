#Creating a Simple RESTful Web App with Node.js, Express, and MongoDB

A sample project for sbi developer candidates with some basic REST functionality for a user api.

## Getting Started

### Prerequirements

- [Node.js](https://nodejs.org/en/download/)
- [Mongodb](https://www.mongodb.org/downloads#production)

### Preparation

- Clone repository to working directory
- Create folder data in working directory
- run `npm install` in the working directory
- run `mongod --dbpath {working directory}/data` to start up the mongodb
- run `npm start` and browse to http://localhost:3000 to see the simple interface
- run `npm test` to run tests

### To Do

- Make all tests pass consistently
- Add more robust error handling for each route
- add any new tests that can be helpful
- add a modern front end to the api you just fixed. For example, use React, Vue, Angular or Ember to replace the jade server side templates. We do use React so using React will allow us to evaluate your code to the best of our ability, but use whatever Framework/Library you feel most comfortable in.

## Contents

- /public - static directories suchs as /images
- /routes - route files for tutorial project
- /views - views for tutorial project
- /test - mocha integration tests
- README.md - this file
- app.js - central app file for tutorial project
- package.json - package info for tutorial project

## Issues

- please don't hesitate to email [darryl@sbiteam.com](mailto:darryl@sbiteam.com) with any questions or issues you may have

## To Run Angular Front-end

- Clone repository to working directory
- Create folder data in working directory
- run `npm install` in the working directory
- run `npm install cors` in the working directory
- run `mongod --dbpath {working directory}/data` to start up the mongodb
- run `npm start` and browse to http://localhost:3000 to see the simple interface
- first follow the instructions found in the "Preparation" section above
- run `ng build` in the client directory found within the working directory
- run `ng serve` in the client directory and browse to http://localhost:4200 to see the angular interface