import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { map } from 'rxjs/operators';

import "rxjs"

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserListComponent implements OnInit {
  users: User[] = [];
  userInfo = {
  }
  newUser = {
    username: '',
    email: '',
    fullname: '',
    age: '',
    location: '',
    gender: ''
  }

  constructor(private _userService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    console.log('Getting Users');
    let observable = this._userService.allUsers();
    observable.subscribe(data => {
      this.users = data.json();
    })
  }

  deleteUser(userID, idx){
    if (confirm('Are you sure you want to delete this?')) {
      this._userService.deleteUser(userID);
      this.users.splice(idx, 1);
    }
    
  }

  userDetail(idx){
    this.userInfo = this.users[idx];
  }

  onSubmit(){
    let observable = this._userService.addUser(this.newUser);
    this.users.push(this.newUser);
    this.newUser = {
      username: '',
      email: '',
      fullname: '',
      age: '',
      location: '',
      gender: ''
    };
  }

}