import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { User } from './user';

@Injectable()

export class UserService {

  constructor(private _http: Http) {
   }

  allUsers() {
    return this._http.get('http://localhost:3000/users/userlist');
  }

  deleteUser(id) {
    console.log('attempting to delete user', id)
    this._http.delete('http://localhost:3000/users/deleteuser/' + id).toPromise();
    console.log('tried to delete');
  }

  addUser(newUser: User) {
    console.log('Adding user', newUser);
    this._http.post('http://localhost:3000/users/adduser', newUser).toPromise();
  }
}
