var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");

var url = 'http://localhost:3000';

describe('Add User', function(){
	it('Adds a new user with user name \'test user\'', function(done){
		var newUser = {
			'username' : '',
			'email' : '',
			'fullname' : '',
			'age' : '',
			'location' : '',
			'gender' : ''
			};
		
		request(url)
		.post('/users/adduser')
		.send(newUser)
		.expect(500)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			done();
		});
	});
});