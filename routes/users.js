var express = require('express');
var router = express.Router();

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.find({},{},function(err, docs){
        (err === null) ? res.json(docs) : { msg: 'Failed to retrieve userlist.  Error:' + err };
    });
});

/*
 * POST to adduser.
 */
router.post('/adduser', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var errorList = '';
    if(req.body.username === ''){
        errorList +='Missing username. ';
    };
    if(req.body.email === ''){
        errorList +='Missing email. ';
    };
    if(req.body.fullname === ''){
        errorList +='Missing Full Name. ';
    };
    if(req.body.age === ''){
        errorList +='Missing age. ';
    };
    if(req.body.location === ''){
        errorList +='Missing location. ';
    };
    if(req.body.gender === ''){
        errorList +='Missing gender. ';
    };
    if(errorList === '' ){
        collection.insert(req.body, function(err, result){
            res.status(201);
            res.send(
                (err === null) ? { msg: '' } : { msg: 'Failed to adduser.  Error: ' + err }
            );
        });
    } else{
        console.log(errorList);
        res.status(500).json(errorList);
    };
    
});

/*
 * PUT to updateuser
 */
router.put('/updateuser/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userToUpdate = req.params.id;
    collection.update({'_id' : userToUpdate}, req.body, function(err) {
        res.send((err === null) ? { msg: 'Updated the user record' } : { msg:'Failed to udpate user. Error: ' + err });
    });
});


/*
 * DELETE to deleteuser.
 */
router.delete('/deleteuser/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userToDelete = req.params.id;
    collection.remove({ '_id' : userToDelete }, function(err) {
        res.send((err === null) ? { msg: 'User deleted' } : { msg:'Failed to delete user. Error: ' + err });
    });
});

module.exports = router;